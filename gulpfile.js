const gulp = require("gulp"),
	sass = require("gulp-sass"),
	browserSync = require("browser-sync"),
	imagemin = require("gulp-imagemin"),
	htmlreplace = require("gulp-html-replace"),
	concat = require("gulp-concat"),
	concatCss = require("gulp-concat-css"),
	cache = require("gulp-cache"),
	del = require("del");

const style_src = "./src/scss/**/*.scss",
	style_dist = "./dist/css/",
	js_src = "./src/js/**/*.js",
	js_dist = "./dist/js/";

function html() {
	return gulp
		.src("./src/*.html")
		.pipe(
			htmlreplace({
				js: {
					src: "js",
					tpl: '<script src="%s/bundle.js"></script>'
				},
				css: {
					src: "css",
					tpl: '<link rel="stylesheet" type="text/css" href="%s/bundle.css" />'
				}
			})
		)
		.pipe(gulp.dest("./dist/"));
}

function scss() {
	return gulp
		.src([style_src, "!./src/scss/base/*"])
		.pipe(sass({ errorLogToConsole: true }))
		.on("error", console.error.bind(console))
		.pipe(gulp.dest(style_dist))
		.pipe(concatCss("bundle.css"))
		.pipe(gulp.dest("./dist/css"))
		.pipe(browserSync.stream());
}

function js() {
	return (
		gulp
			.src(js_src)
			// copy js chunks for reference
			.pipe(gulp.dest(js_dist))
			// bundle all js chunks without minified for dev
			.pipe(concat("bundle.js"))
			// export it to js dir again
			.pipe(gulp.dest("./dist/js"))
	);
}

function images() {
	return gulp
		.src("src/img/**/*.+(png|jpg|jpeg|gif|svg)")
		.pipe(
			cache(
				imagemin({
					interlaced: true
				})
			)
		)
		.pipe(gulp.dest("./dist/img"));
}

function watch() {
	browserSync.init({
		server: {
			baseDir: "./dist"
		}
	});
	gulp.watch(style_src, gulp.series(scss));
	gulp.watch("./src/js/**/*.js").on("all", function(done) {
		js();
		browserSync.reload();
	});
	gulp.watch("./src/*.html").on("all", function(done) {
		html();
		browserSync.reload();
	});
	gulp.watch("src/img/**/*.+(png|jpg|jpeg|gif|svg)").on("all", function(done) {
		images();
		browserSync.reload();
	});
}

const clean = function() {
	return del("dist");
};

const cleanSpec = function() {
	return del(["dist/**/*", "!dist/images", "!dist/images/**/*"], callback);
};

exports.html = html;
exports.scss = scss;
exports.js = js;
exports.images = images;
exports.watch = watch;
exports.clean = clean;
exports.cleanSpec = cleanSpec;

const transiple = gulp.series(clean, html, scss, js, images);

gulp.task("dev", gulp.series(clean, gulp.parallel(transiple, watch)));
gulp.task("build", gulp.series(clean, transiple));
